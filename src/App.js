import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';

import Reducers from './reducers';
import RouterComponent from './Router';

class App extends Component {
   render() {
      const store = createStore(Reducers, {}, applyMiddleware(ReduxThunk));

      return (
         <Provider store={store}>
            <RouterComponent />
         </Provider>
      );
   }
}

export default App;
