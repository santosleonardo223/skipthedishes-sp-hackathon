import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ListView } from 'react-native';
import { connect } from 'react-redux';

import ListItem from './ListItem';
import { Card, CardSession, SearchBar, Spinner } from './common';
import * as actions from '../actions/StoresActions';

class StoresComponent extends Component {
   constructor(props) {
      super(props);

      const ds = new ListView.DataSource({
         rowHasChanged: (r1, r2) => r1 !== r2
      });

      this.state = { ds };
      this.renderList = this.renderList.bind(this);
      this.renderRow = this.renderRow.bind(this);
   }

   componentWillMount() {
      this.props.searchStores();
   }

   searchStoreByName() {
      this.props.searchStore(this.props.search);
   }

   renderRow(restaurant) {
      return <ListItem restaurant={restaurant} isRestaurant={this.props.isRestaurant} />;
   }

   renderList() {
      if (this.props.load) {
         return (
            <CardSession>
               <Spinner size='large' />
            </CardSession>
         );
      }

      return (
         <CardSession>
            <ListView
               dataSource={this.state.ds.cloneWithRows(this.props.restaurants)}
               renderRow={this.renderRow}
            />
         </CardSession>
      );
   }

   render() {
      const { menuContainerStyle, buttonStyle, menuTextStyle } = styles;

      return (
         <View style={{ flex: 1 }}>
            <Card>
               <View style={menuContainerStyle}>
                  <TouchableOpacity
                     onPress={() => this.props.searchStores()}
                     style={buttonStyle}
                  >
                     <Text style={menuTextStyle}>Stores</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                     onPress={() => this.props.searchCousines()}
                     style={buttonStyle}
                  >
                     <Text style={menuTextStyle}>Cousines</Text>
                  </TouchableOpacity>
               </View>
            </Card>

            <Card>
               <CardSession>
                  <SearchBar
                     placeholder='search'
                     value={this.props.search}
                     updateText={text => this.props.updateSearchBar(text)}
                     search={this.searchStoreByName.bind(this)}
                  />
               </CardSession>
            </Card>

            <Card>
               { this.renderList() }
            </Card>
         </View>
      );
   }
}

const styles = {
   menuContainerStyle: {
      height: 40,
      backgroundColor: '#ffffff',
      flexDirection: 'row',
      alignItems: 'center'
   },

   buttonStyle: {
     flex: 1,
     alignSelf: 'stretch',
     backgroundColor: '#fff',
     marginLeft: 5,
     marginRight: 5
  },

   menuTextStyle: {
      alignSelf: 'center',
      fontSize: 20,
      fontWeight: '600',
      paddingTop: 5,
      paddingBottom: 5
   },
};

const mapStateToProps = state => ({
   restaurants: state.stores.stores,
   search: state.stores.search,
   load: state.stores.load,
   isRestaurant: state.stores.isRestaurant
});

export default connect(mapStateToProps, actions)(StoresComponent);
