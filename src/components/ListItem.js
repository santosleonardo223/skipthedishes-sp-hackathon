import React, { Component } from 'react';
import { Text, View, TouchableWithoutFeedback } from 'react-native';

class ListItem extends Component {
   renderListText() {
      const { name, address } = this.props.restaurant;
      const { listItemContainer, titleStyle } = styles;

      if (this.props.isRestaurant) {
         return (
            <View style={listItemContainer}>
               <Text style={titleStyle}>{name}</Text>
               <Text>{address}</Text>
            </View>
         );
      }

      return (
         <View style={listItemContainer}>
            <Text style={titleStyle}>{name}</Text>
         </View>
      );
   }

   render() {
      return (
         <TouchableWithoutFeedback onPress={() => console.log(this.props.restaurant.name)}>
            { this.renderListText() }
         </TouchableWithoutFeedback>
      );
   }
}

const styles = {
   listItemContainer: {
      borderBottomWidth: 1,
      padding: 5,
      backgroundColor: '#ffffff',
      justifyContent: 'flex-start',
      borderColor: '#ddd',
      position: 'relative'
   },

   titleStyle: {
      fontSize: 18,
      fontWeight: '700'
   }
};

export default ListItem;
