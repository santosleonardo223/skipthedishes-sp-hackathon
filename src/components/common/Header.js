import React from 'react';
import { View, Text } from 'react-native';

const Header = (props) => {
    const { viewStyle, titleStyle } = styles;

    return (
        <View style={viewStyle}>
            <Text style={titleStyle}>{props.title}</Text>
        </View>
    );
};

const styles = {
    viewStyle: {
        height: 60,
        backgroundColor: '#F8F8F8',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 15,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },

    titleStyle: {
        fontSize: 20,
    }
};

export { Header };
