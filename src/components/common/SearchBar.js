import React from 'react';
import { TextInput, View } from 'react-native';

import { Button } from './Button';

const SearchBar = (props) => {
   const { containerStyle, inputStyle } = styles;

   return (
      <View style={containerStyle}>
         <TextInput
            autoCorrect={false}
            placeholder={props.placeholder}
            value={props.value}
            onChangeText={text => props.updateText(text)}
            style={inputStyle}
         />

      <Button onPress={props.search}>
            Search
         </Button>
      </View>
   );
};

const styles = {
    containerStyle: {
        height: 40,
        justifyContent: 'space-around',
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },

    inputStyle: {
        color: '#000',
        paddingRight: 5,
        paddingLeft: 5,
        fontSize: 18,
        lineHeight: 23,
        flex: 2,
    }
};

export { SearchBar };
