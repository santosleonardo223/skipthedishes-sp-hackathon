import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { Card, CardSession, InputText, Button, Spinner } from './common';
import { updateUserEmail, updateUserPassword, loginUser } from '../actions/AuthActions';

class Login extends Component {
   renderButtonOrSpinner() {
      const { email, password, error } = this.props;


      if (this.props.load) {
         return (
            <CardSession>
               <Spinner size='large' />
            </CardSession>
         );
      }

      return (
         <View>
            <CardSession>
               <Text style={styles.errorStyle}>
                  {error}
               </Text>
            </CardSession>

            <CardSession>
               <Button onPress={() => this.props.loginUser(email, password)}>
                  Login
               </Button>
            </CardSession>

            <CardSession>
               <Button onPress={() => Actions.signUp()}>
                  Sign Up
               </Button>
            </CardSession>
         </View>
      );
   }

   render() {
      const { email, password } = this.props;

      return (
         <Card>
            <CardSession>
               <InputText
                  label='Email'
                  placeholder='Leonardo'
                  updateText={userEmail => this.props.updateUserEmail(userEmail)}
                  value={email}
               />
            </CardSession>

            <CardSession>
               <InputText
                  secureText
                  label='Password'
                  placeholder='password'
                  updateText={userPassword => this.props.updateUserPassword(userPassword)}
                  value={password}
               />
            </CardSession>

            {this.renderButtonOrSpinner()}
         </Card>
      );
   }
}

const styles = {
   errorStyle: {
      fontSize: 18,
      color: 'red',
   }
};

const mapStateToProps = state => ({
   email: state.auth.email,
   password: state.auth.password,
   error: state.auth.error,
   load: state.auth.load
});

export default connect(mapStateToProps, { updateUserEmail, updateUserPassword, loginUser })(Login);
