import React from 'react';
import { Router, Scene, Stack } from 'react-native-router-flux';

import Login from './components/Login';
import SignUp from './components/SignUp';
import Stores from './components/Stores';

const RouterComponent = () => (
   <Router>
      <Stack hey='root' hideNavBar>
         <Scene key='auth'>
            <Scene key='login' component={Login} title='Login' initial />
            <Scene key='signUp' component={SignUp} title='Sign Up' />
         </Scene>

         <Scene key='main'>
            <Scene key='stores' component={Stores} title='Stores' />
         </Scene>
      </Stack>
   </Router>
);

export default RouterComponent;
