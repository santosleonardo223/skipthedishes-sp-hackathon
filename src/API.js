export const HOST : String = 'http://api-vanhack-event-sp.azurewebsites.net';
export const API_CUSTOMER_AUTH : String = '/api/v1/Customer/auth';
export const API_GET_STORES : String = '/api/v1/Store';
export const API_GET_STORE_BY_NAME : String = '/api/v1/Store/search/';
export const API_GET_COUSINES : String = '/api/v1/Cousine';
