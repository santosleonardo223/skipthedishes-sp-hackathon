import {
   UPDATE_EMAIL,
   UPDATE_PASSWORD,
   TRYING_TO_LOGIN,
   LOGIN_USER_SUCCESS,
   LOGIN_USER_FAIL
} from '../actions/types';

const INITIAL_STATE = { email: '', password: '', load: false, error: '', authCode: '' };

export default (state = INITIAL_STATE, action) => {
   console.log(action.type);

   switch (action.type) {
      case UPDATE_EMAIL:
         return { ...state, email: action.payload };

      case UPDATE_PASSWORD:
         return { ...state, password: action.payload };

      case TRYING_TO_LOGIN:
         return { ...state, load: true, error: '' };

      case LOGIN_USER_SUCCESS:
         return { ...state, ...INITIAL_STATE, authCode: action.payload };

      case LOGIN_USER_FAIL:
         return { ...state, error: 'Failed to login.', load: false };

      default:
         return state;
   }
};
