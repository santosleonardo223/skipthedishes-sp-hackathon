import { combineReducers } from 'redux';

import AuthReducer from './AuthReducer';
import StoresReducer from './StoresReducer';

export default combineReducers({
   auth: AuthReducer,
   stores: StoresReducer
});
