import { UPDATE_SEARCH_BAR, UPDATE_STORES, UPDATE_SPINNER } from '../actions/types';

const INITIAL_STATE = { stores: [], search: '', load: true, isRestaurant: true };

export default (state = INITIAL_STATE, action) => {
   console.log(action.type);

   switch (action.type) {
      case UPDATE_SPINNER:
         return {
            ...state,
            load: action.payload.load,
            isRestaurant: action.payload.isRestaurant,
            search: ''
         };

      case UPDATE_SEARCH_BAR:
         return { ...state, search: action.payload };

      case UPDATE_STORES:
         return { ...state, stores: action.payload, load: false };

      default:
         return state;
   }
};
