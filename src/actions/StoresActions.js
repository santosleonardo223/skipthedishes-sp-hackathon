import { HOST, API_GET_STORES, API_GET_STORE_BY_NAME, API_GET_COUSINES } from '../API';
import {
   UPDATE_SEARCH_BAR,
   UPDATE_STORES,
   UPDATE_SPINNER
} from './types';

export const updateSearchBar = (text) => (
   {
      type: UPDATE_SEARCH_BAR,
      payload: text
   }
);

export const searchStores = () => {
   return (dispatch) => {
      fetch(HOST + API_GET_STORES)
         .then(res => res.json())
         .then(result => {
            dispatch({ type: UPDATE_STORES, payload: result });
         })
         .catch(error => console.log(error));
   };
};

export const searchStore = (storeName) => {
   return (dispatch) => {
      dispatch({
         type: UPDATE_SPINNER,
         payload: { load: true, isRestaurant: true }
      });

      console.log(HOST + API_GET_STORE_BY_NAME + storeName);
      fetch(HOST + API_GET_STORE_BY_NAME + storeName)
         .then(res => res.json())
         .then(result => {
            dispatch({ type: UPDATE_STORES, payload: result });
         })
         .catch(error => console.log(error));
   };
};

export const searchCousines = () => {
   return (dispatch) => {
      dispatch({
         type: UPDATE_SPINNER,
         payload: { load: true, isRestaurant: false }
      });

      fetch(HOST + API_GET_COUSINES)
         .then(res => res.json())
         .then(result => {
            dispatch({ type: UPDATE_STORES, payload: result });
         })
         .catch(error => console.log(error));
   };
}
