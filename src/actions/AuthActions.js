import { Actions } from 'react-native-router-flux';

import { HOST, API_CUSTOMER_AUTH } from '../API';
import {
   UPDATE_EMAIL,
   UPDATE_PASSWORD,
   TRYING_TO_LOGIN,
   LOGIN_USER_SUCCESS,
   LOGIN_USER_FAIL
} from './types';

export const updateUserEmail = (email) => (
   {
      type: UPDATE_EMAIL,
      payload: email
   }
);

export const updateUserPassword = (password) => (
   {
      type: UPDATE_PASSWORD,
      payload: password
   }
);

export const loginUser = (email, password) => {
   return (dispatch) => {
      dispatch({ type: TRYING_TO_LOGIN });

      const details = {
         email: email,
         password: password
      };

      let formBody = [];
      for (let property in details) {
         const encodedKey = encodeURIComponent(property);
         const encodedValue = encodeURIComponent(details[property]);
         formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");

      fetch(HOST + API_CUSTOMER_AUTH, {
         method: 'POST',
         headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
         },
         body: formBody
      })
         .then(
            (response) => {
               if (response.status === 200) {
                  loginUserSuccess(dispatch, response);
               } else {
                  loginUserFail(dispatch);
               }
            }
         )
         .catch(error => console.log(error));
   };
};

const loginUserSuccess = (dispatch, response) => {
   console.log('AUTH CODE RESPONSE:');
   console.log(response);

   dispatch({
      type: LOGIN_USER_SUCCESS,
      payload: response
   });

   Actions.main();
};

const loginUserFail = (dispatch) => {
   dispatch({ type: LOGIN_USER_FAIL });
};
